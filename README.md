# Chip8 Emulator in C

Just another Chip8 emulator written in C99 using SDL2 for display and input. Games seem to work fine but some test ROMs are giving me a few issues. In the future I plan to add Super Chip 8 support, audio support, and figure out any other remaining implementation problems.