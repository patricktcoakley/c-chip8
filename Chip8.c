#include "Chip8.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

const uint16_t CHIP8_START_ADDRESS = 0x200;
const uint8_t FONT_START_ADDRESS = 0x50;
const uint8_t VIDEO_HEIGHT = 32;
const uint8_t VIDEO_WIDTH = 64;
const uint32_t ON_COLOR = 0xFFF0000;
const uint32_t OFF_COLOR = 0x0;
const uint8_t CHAR_SIZE = 5;
const uint8_t FONTS[] = {
	0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
	0x20, 0x60, 0x20, 0x20, 0x70, // 1
	0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
	0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
	0x90, 0x90, 0xF0, 0x10, 0x10, // 4
	0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
	0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
	0xF0, 0x10, 0x20, 0x40, 0x40, // 7
	0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
	0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
	0xF0, 0x90, 0xF0, 0x90, 0x90, // A
	0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
	0xF0, 0x80, 0x80, 0x80, 0xF0, // C
	0xE0, 0x90, 0x90, 0x90, 0xE0, // D
	0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
	0xF0, 0x80, 0xF0, 0x80, 0x80  // F
};

void PlayAudio(); // TODO: Add sound

void LoadRom(Chip8* chip8, const char* path)
{
	FILE* f;
	if (!(f = fopen(path, "r")))
	{
		printf("Failed to load %s\n", path);
		exit(EXIT_FAILURE);
	}

	fseek(f, 0, SEEK_END);
	long size = ftell(f);
	rewind(f);

	char buffer[size];
	memset(buffer, 0, size);
	fread(buffer, 1, size, f);
	fclose(f);
	memcpy(chip8->Memory + CHIP8_START_ADDRESS, buffer, size);
	memcpy(chip8->Memory + FONT_START_ADDRESS, FONTS, sizeof(FONTS));
	chip8->PC = CHIP8_START_ADDRESS;
	srand(time(0));
}

void Cycle(Chip8* chip8)
{
	chip8->Opcode = chip8->Memory[chip8->PC] << 8 | chip8->Memory[chip8->PC + 1];
	chip8->PC += 2;

	const uint16_t vx = (chip8->Opcode & 0x0F00) >> 8;
	const uint16_t vy = (chip8->Opcode & 0x00F0) >> 4;
	const uint8_t n = chip8->Opcode & 0x000F;
	const uint16_t nn = chip8->Opcode & 0x00FF;
	const uint16_t nnn = chip8->Opcode & 0x0FFF;

	switch (chip8->Opcode & 0xF000)
	{
	case 0x0000:
		switch (n)
		{
		case 0x00: // 00EO: CLS - Clear screen
			memset(chip8->Video, OFF_COLOR, sizeof(chip8->Video));
			break;
		case 0x0E: // 00EE: RET - Return
			chip8->PC = chip8->Stack[--chip8->SP];
			break;
		default:
			printf("Unknown 0x0 Opcode 0x%08x\n", chip8->Opcode & 0xF000);
			break;
		}
		break;
	case 0x1000: // 1NNN: JP addr - Jump to address
		chip8->PC = nnn;
		break;
	case 0x2000: // 2NNN: CALL addr - Call subroutine at address
		chip8->Stack[chip8->SP++] = chip8->PC;
		chip8->PC = nnn;
		break;
	case 0x3000: // 3XKK: SE Vx, byte - Skip next instruction if Vx = byte
		if (chip8->Registers[vx] == nn)
		{
			chip8->PC += 2;
		}
		break;
	case 0x4000: // 4XKK: SNE Vx, byte - Skip next instruction if Vx != byte
		if (chip8->Registers[vx] != nn)
		{
			chip8->PC += 2;
		}
		break;
	case 0x5000: // 5XY0: SE Vx, Vy - Skip next instruction if Vx = Vy
		if (chip8->Registers[vx] == chip8->Registers[vy])
		{
			chip8->PC += 2;
		}
		break;
	case 0x6000: // 6XKK: LD Vx, byte - Set Vx = byte
		chip8->Registers[vx] = nn;
		break;
	case 0x7000: // 7XKK: ADD Vx, byte - Add byte to Vx
		chip8->Registers[vx] += nn;
		break;
	case 0x8000:
		switch (n)
		{
		case 0x00: //8XY0: LD Vx, Vy - Set Vx = Vy
			chip8->Registers[vx] = chip8->Registers[vy];
			break;
		case 0x01: //8XY1: OR Vx, Vy - Set Vx = Vx OR Vy
			chip8->Registers[vx] |= chip8->Registers[vy];
			break;
		case 0x02: //8XY2: AND Vx, Vy - Set Vx = Vx AND Vy
			chip8->Registers[vx] &= chip8->Registers[vy];
			break;
		case 0x03: //8XY3: XOR Vx, Vy - Set Vx = Vx XOR Vy
			chip8->Registers[vx] ^= chip8->Registers[vy];
			break;
		case 0x04: // 8XY4: ADD Vx, Vy - Set Vx = Vx + Vy, Set VF = carry
		{
			uint16_t result = chip8->Registers[vx] + chip8->Registers[vy];
			chip8->Registers[0xF] = result > 0xFF ? 1 : 0;
			chip8->Registers[vx] = result;
		}
			break;
		case 0x05: // 8XY5: SUB Vx, Vy - Set Vx = Vx - Vy, Set VF = not borrow
			chip8->Registers[0xF] = chip8->Registers[vx] > chip8->Registers[vy] ? 1 : 0;
			chip8->Registers[vx] -= chip8->Registers[vy];
			break;
		case 0x06: // 8XY6: SHR Vx - Set Vx = Vx SHR 1
			chip8->Registers[0xF] = chip8->Registers[vx] & 1;
			chip8->Registers[vx] >>= 1;
			break;
		case 0x07: // 8XY7: SUB Vx, Vy - Set Vx = Vy - Vx, Set VF = not borrow
			chip8->Registers[0xF] = chip8->Registers[vx] < chip8->Registers[vy] ? 1 : 0;
			chip8->Registers[vx] = chip8->Registers[vy] - chip8->Registers[vx];
			break;
		case 0x0E: // 8XY6: SHL Vx - Set Vx = Vx SHL 1
			chip8->Registers[0xF] = (chip8->Registers[vx] & 0x80) >> 7;
			chip8->Registers[vx] <<= 1;
			break;
		default:
			printf("Unknown 0x8 Opcode 0x%08x\n", chip8->Opcode & 0xF000);
			break;
		}
		break;
	case 0x9000: // 9XY0: SNE Vx, Vy - Skip next instruction if Vx != Vy
		if (chip8->Registers[vx] != chip8->Registers[vy])
		{
			chip8->PC += 2;
		}
		break;
	case 0xA000: // ANNN: LD I, addr - Set I = nnn
		chip8->I = nnn;
		break;
	case 0xB000: // BNNN: JP V0, addr - Jump to address V0 + addr
		chip8->PC = chip8->Registers[nnn];
		break;
	case 0xC000: // CXKK: RND Vx, byte - Set Vx = random byte AND byte
		chip8->Registers[vx] = (rand() % 0xFF) & nn;
		break;
	case 0xD000: // DXYN: DRW Vx, Vy, nibble - Display n-byte sprite starting at I to coordinates (Vx, Vy), Set VF = collision
	{
		uint8_t x_pos = chip8->Registers[vx] % VIDEO_WIDTH;
		uint8_t y_pos = chip8->Registers[vy] % VIDEO_HEIGHT;
		chip8->Registers[0xF] = 0;
		for (int y = 0; y < n; ++y)
		{
			for (int x = 0; x < 8; ++x)
			{
				if (chip8->Memory[chip8->I + y] & (0x80 >> x))
				{
					uint32_t* screen_pos = &chip8->Video[(y_pos + y) * VIDEO_WIDTH + (x_pos + x)];
					if (*screen_pos)
					{
						chip8->Registers[0xF] = 1;
						*screen_pos = OFF_COLOR;
					}
					else
					{
						*screen_pos = ON_COLOR;
					}
				}
			}
		}
	}
		break;
	case 0xE000:
		switch (nn)
		{
		case 0x9E:  // EX9E: SKP Vx - Skip next instruction if key with the value of Vx is pressed
		{
			uint8_t keycode = chip8->Registers[vx];

			if (chip8->Keypad[keycode])
			{
				chip8->PC += 2;
			}
		}
			break;
		case 0xA1: // ExA1: SKNP Vx - Skip next instruction if key with the value of Vx is not pressed
		{
			uint8_t keycode = chip8->Registers[vx];

			if (!chip8->Keypad[keycode])
			{
				chip8->PC += 2;
			}
		}
			break;
		default:
			printf("Unknown 0xE Opcode 0x%08x\n", chip8->Opcode & 0xF000);
			break;
		}
		break;
	case 0xF000:
		switch (nn)
		{
		case 0x07: // FX07: LD Vx, DT - Set Vx = delay timer
			chip8->Registers[vx] = chip8->DelayTimer;
			break;
		case 0x0A: // FX0A: LD Vx, K - Wait for key press and store the value into Vx
			if (chip8->Keypad[0])
			{
				chip8->Registers[vx] = 0;
			}
			else if (chip8->Keypad[1])
			{
				chip8->Registers[vx] = 1;
			}
			else if (chip8->Keypad[2])
			{
				chip8->Registers[vx] = 2;
			}
			else if (chip8->Keypad[3])
			{
				chip8->Registers[vx] = 3;
			}
			else if (chip8->Keypad[4])
			{
				chip8->Registers[vx] = 4;
			}
			else if (chip8->Keypad[5])
			{
				chip8->Registers[vx] = 5;
			}
			else if (chip8->Keypad[6])
			{
				chip8->Registers[vx] = 6;
			}
			else if (chip8->Keypad[7])
			{
				chip8->Registers[vx] = 7;
			}
			else if (chip8->Keypad[8])
			{
				chip8->Registers[vx] = 8;
			}
			else if (chip8->Keypad[9])
			{
				chip8->Registers[vx] = 9;
			}
			else if (chip8->Keypad[10])
			{
				chip8->Registers[vx] = 10;
			}
			else if (chip8->Keypad[11])
			{
				chip8->Registers[vx] = 11;
			}
			else if (chip8->Keypad[12])
			{
				chip8->Registers[vx] = 12;
			}
			else if (chip8->Keypad[13])
			{
				chip8->Registers[vx] = 13;
			}
			else if (chip8->Keypad[14])
			{
				chip8->Registers[vx] = 14;
			}
			else if (chip8->Keypad[15])
			{
				chip8->Registers[vx] = 15;
			}
			else
			{
				chip8->PC -= 2;
			}
			break;
		case 0x15:  // FX15: LD DT, Vx - Set delay timer = Vx
			chip8->DelayTimer = chip8->Registers[vx];
			break;
		case 0x18: // FX18: LD ST, Vx - Set sound timer = Vx
			chip8->SoundTimer = chip8->Registers[vx];
			break;
		case 0x1E: // FX1E: Add I, Vx - Set I = I + Vx
			chip8->I += chip8->Registers[vx];
			break;
		case 0x29: // FX29: LD F, Vx - Set I = location of sprite for digit Vx
			chip8->I = FONT_START_ADDRESS + (CHAR_SIZE * chip8->Registers[vx]);
			break;
		case 0x33: // FX33: LD B, Vx - Store BCD (Binary-Coded Decimal) representation of Vx in memory locations I, I + 1, and I + 2
		{
			uint8_t result = chip8->Registers[vx];
			chip8->Memory[chip8->I + 2] = result % 10;
			result /= 10;
			chip8->Memory[chip8->I + 1] = result % 10;
			result /= 10;
			chip8->Memory[chip8->I] = result % 10;
		}
			break;
		case 0x55: // FX55: LD [I], Vx - Store V0~Vx in memory starting at location I
		for (size_t offset = 0; offset <= vx; ++offset)
			{
				chip8->Memory[chip8->I + offset] = chip8->Registers[offset];
			}
			break;
		case 0x65: // FX65: LD Vx, [I] - Read registers V0~Vx from memory starting at location I
			for (size_t offset = 0; offset <= vx; ++offset)
			{
				chip8->Registers[offset] = chip8->Memory[chip8->I + offset];
			}
			break;
		default:
			printf("Unknown 0xF opcode: %d\n", chip8->Opcode);
		}

		break;

	default:
		printf("Unknown Opcode 0x%08x\n", chip8->Opcode & 0xF000);
		break;
	}

	if (chip8->DelayTimer > 0)
	{
		--chip8->DelayTimer;
	}

	if (chip8->SoundTimer > 0)
	{
		--chip8->SoundTimer;
	}
}