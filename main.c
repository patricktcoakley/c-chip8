#include "Application.h"

int main(int argc, char** argv)
{
	if (argc != 2)
	{
		printf("Usage: Chip8 <TITLE>\n");
		exit(EXIT_FAILURE);
	}

	Application application;
	InitApplication(&application);
	Run(&application, argv[1]);
	Quit(&application);
	return 0;
}
