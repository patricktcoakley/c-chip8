#ifndef CHIP8__APPLICATION_H
#define CHIP8__APPLICATION_H
#include "Chip8.h"
#include <SDL2/SDL.h>
#include <stdbool.h>

typedef struct {
	SDL_Window* Window;
	SDL_Renderer* Renderer;
	SDL_Texture* Texture;
	Chip8 Chip8;
} Application;

void InitApplication(Application* application);
void Run(Application* application, const char* title);
void Quit(Application* application);

#endif //CHIP8__APPLICATION_H
