#include "Application.h"

void Update(Application* application);
bool ProcessInput(uint8_t* keys);

void InitApplication(Application* application)
{
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		printf("Failed to initialize: %s", SDL_GetError());
		exit(EXIT_FAILURE);
	}

	application->Window = SDL_CreateWindow(
		"Chip8",
		SDL_WINDOWPOS_CENTERED,
		SDL_WINDOWPOS_CENTERED,
		1280,
		640,
		SDL_WINDOW_SHOWN
	);

	if (!application->Window)
	{
		printf("Failed to initialize: %s", SDL_GetError());
		exit(EXIT_FAILURE);
	}

	application->Renderer = SDL_CreateRenderer(
		application->Window,
		-1,
		SDL_RENDERER_ACCELERATED
	);

	if (!application->Renderer)
	{
		printf("Failed to initialize: %s", SDL_GetError());
		exit(EXIT_FAILURE);
	}

	application->Texture = SDL_CreateTexture(
		application->Renderer,
		SDL_PIXELFORMAT_RGBA8888,
		SDL_TEXTUREACCESS_STREAMING,
		VIDEO_WIDTH,
		VIDEO_HEIGHT
	);

	if (!application->Texture)
	{
		printf("Failed to initialize: %s", SDL_GetError());
		exit(EXIT_FAILURE);
	}

	memset(&application->Chip8, 0, sizeof(Chip8));
}

void Quit(Application* application)
{
	SDL_DestroyTexture(application->Texture);
	SDL_DestroyRenderer(application->Renderer);
	SDL_DestroyWindow(application->Window);
	SDL_Quit();
}

void Run(Application* application, const char* title)
{
	char path[256] = { 0 };
	sprintf(path, "roms/%s", title);
	LoadRom(&application->Chip8, path);
	SDL_SetWindowTitle(application->Window, title);
	bool finished = false;
	uint32_t last = 0;
	uint32_t current;
	while (!finished)
	{
		finished = ProcessInput(application->Chip8.Keypad);
		current = SDL_GetTicks() * 540;
		if (current > last + 1000)
		{
			last = current;
			Cycle(&application->Chip8);
			Update(application);
		}
	}
}

void Update(Application* application)
{
	SDL_UpdateTexture(application->Texture, NULL, &application->Chip8.Video, 256);
	SDL_RenderClear(application->Renderer);
	SDL_RenderCopy(application->Renderer, application->Texture, NULL, NULL);
	SDL_RenderPresent(application->Renderer);
}

bool ProcessInput(uint8_t* keys)
{
	bool finished = false;
	SDL_Event event;
	while (SDL_PollEvent(&event))
	{
		switch (event.type)
		{
		case SDL_QUIT:
			finished = true;
			break;
		case SDL_KEYDOWN:
		{
			switch (event.key.keysym.sym)
			{
			case SDLK_ESCAPE:
				finished = true;
				break;
			case SDLK_x:
				keys[0] = 1;
				break;
			case SDLK_1:
				keys[1] = 1;
				break;
			case SDLK_2:
				keys[2] = 1;
				break;
			case SDLK_3:
				keys[3] = 1;
				break;
			case SDLK_q:
				keys[4] = 1;
				break;
			case SDLK_w:
				keys[5] = 1;
				break;
			case SDLK_e:
				keys[6] = 1;
				break;
			case SDLK_a:
				keys[7] = 1;
				break;
			case SDLK_s:
				keys[8] = 1;
				break;
			case SDLK_d:
				keys[9] = 1;
				break;
			case SDLK_z:
				keys[0xA] = 1;
				break;
			case SDLK_c:
				keys[0xB] = 1;
				break;
			case SDLK_4:
				keys[0xC] = 1;
				break;
			case SDLK_r:
				keys[0xD] = 1;
				break;
			case SDLK_f:
				keys[0xE] = 1;
				break;
			case SDLK_v:
				keys[0xF] = 1;
				break;
			}
			break;
		}
		case SDL_KEYUP:
		{
			switch (event.key.keysym.sym)
			{
				case SDLK_x:
					keys[0] = 0;
					break;
				case SDLK_1:
					keys[1] = 0;
					break;
				case SDLK_2:
					keys[2] = 0;
					break;
				case SDLK_3:
					keys[3] = 0;
					break;
				case SDLK_q:
					keys[4] = 0;
					break;
				case SDLK_w:
					keys[5] = 0;
					break;
				case SDLK_e:
					keys[6] = 0;
					break;
				case SDLK_a:
					keys[7] = 0;
					break;
				case SDLK_s:
					keys[8] = 0;
					break;
				case SDLK_d:
					keys[9] = 0;
					break;
				case SDLK_z:
					keys[0xA] = 0;
					break;
				case SDLK_c:
					keys[0xB] = 0;
					break;
				case SDLK_4:
					keys[0xC] = 0;
					break;
				case SDLK_r:
					keys[0xD] = 0;
					break;
				case SDLK_f:
					keys[0xE] = 0;
					break;
				case SDLK_v:
					keys[0xF] = 0;
					break;
			}
			break;
		}
		}
	}

	return finished;
}
