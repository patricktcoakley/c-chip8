#ifndef CHIP8__CHIP8_H
#define CHIP8__CHIP8_H
#include <stdint.h>

extern const uint8_t VIDEO_HEIGHT;
extern const uint8_t VIDEO_WIDTH;
extern const uint32_t ON_COLOR;
extern const uint32_t OFF_COLOR;

typedef struct
{
	uint16_t I;
	uint16_t PC;
	uint8_t SP;
	uint16_t Opcode;
	uint8_t DelayTimer;
	uint8_t SoundTimer;
	uint8_t Registers[16];
	uint16_t Stack[16];
	uint8_t Memory[4096];
	uint8_t Keypad[16];
	uint32_t Video[2048];
} Chip8;

void LoadRom(Chip8* chip8, const char* path);
void Cycle(Chip8* chip8);

#endif //CHIP8__CHIP8_H